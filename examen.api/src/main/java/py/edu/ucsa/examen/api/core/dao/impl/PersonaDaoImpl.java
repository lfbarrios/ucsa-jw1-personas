package py.edu.ucsa.examen.api.core.dao.impl;

import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import py.edu.ucsa.examen.api.core.dao.AbstractDao;
import py.edu.ucsa.examen.api.core.dao.PersonaDao;
import py.edu.ucsa.examen.api.core.model.Persona;

@Repository("personaDao")
public class PersonaDaoImpl extends AbstractDao<Integer, Persona> implements PersonaDao {

	@Override
	public Persona getById(Integer id) {
		Persona p = super.getById(id);
		return p;
	}

	@Override
	public Persona getByNumeroDocumento(String nroDoc) {
		logger.debug("Número de documento: " + nroDoc);
		try {
			Persona p = (Persona) getEntityManager().createQuery("SELECT p FROM Persona p WHERE p.nroDoc = :nroDoc")
					.setParameter("nroDoc", nroDoc).getSingleResult();
			return p;
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public void insertar(Persona persona) {
		super.persistir(persona);
	}

	@Override
	public void borrarPorNumeroDocumento(String nroDoc) {
		Persona p = (Persona) getEntityManager().createQuery("SELECT p FROM Persona p WHERE p.nroDoc = :nroDoc")
				.setParameter("nroDoc", nroDoc).getSingleResult();
		eliminar(p);
	}

	@Override
	public void actualizar(Persona persona) {
		super.actualizar(persona);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Persona> listarTodos() {
		List<Persona> persona = getEntityManager().createQuery("SELECT p FROM Persona p ORDER BY p.apellidos ASC")
				.getResultList();
		return persona;
	}

}
