package py.edu.ucsa.examen.api.core.dao;

import java.util.List;
import py.edu.ucsa.examen.api.core.model.TipoDocumento;

public interface TipoDocumentoDao {

	TipoDocumento getById(Integer id);
	
	TipoDocumento getByCodigo(String codigo);

	void insertar(TipoDocumento tipoDocumento);

	void actualizar(TipoDocumento tipoDocumento);

	void borrarPorCodigo(String codigo);

	List<TipoDocumento> listarTodos();

}
