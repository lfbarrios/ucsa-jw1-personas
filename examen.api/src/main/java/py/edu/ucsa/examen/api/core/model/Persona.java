package py.edu.ucsa.examen.api.core.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name = "personas")
@NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p")
public class Persona implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2723985971342319608L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "persona_id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name="tipo_documento_id", nullable=false)
	private TipoDocumento idTipoDoc;//idTipoDoc

	@Column(name = "numero_documento", length = 20, nullable = false)
	private String nroDoc;//nroDoc

	@Column(length = 100, nullable = false)
	private String nombres;

	@Column(length = 100, nullable = true)
	private String apellidos;

	@Column(nullable = false)
	private boolean fisica;

	@Column(length = 250, nullable = true)
	private String direccion;

	@Column(length = 20, nullable = true)
	private String telefono;

	@Column(length = 20, nullable = false)
	private String celular;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public TipoDocumento getidTipoDoc() {
		return idTipoDoc;
	}

	public void setidTipoDoc(TipoDocumento idTipoDoc) {
		this.idTipoDoc = idTipoDoc;
	}

	public String getnroDoc() {
		return nroDoc;
	}

	public void setnroDoc(String nroDoc) {
		this.nroDoc = nroDoc;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public boolean isFisica() {
		return fisica;
	}

	public void setFisica(boolean fisica) {
		this.fisica = fisica;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

}
