package py.edu.ucsa.examen.api.core.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity //@Check(constraints="estado in ('A', 'I'")
@Table(name = "tiposdocumentos")//uniqueConstraints={@UniqueConstraint(columnNames={"codigo","estado"})}
@NamedQuery(name = "TipoDocumento.findAll", query = "SELECT t FROM TipoDocumento t")
public class TipoDocumento implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6795424477908151539L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tipo_documento_id")
	private Integer id;

	@Column(length = 10)
	private String codigo;

	@Column(length = 200)
	private String descripcion;

	@Column(length = 1, nullable = false)
	private char estado;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

}
