package py.edu.ucsa.examen.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages = { "py.edu.ucsa.examen.api.web.controllers",
		"py.edu.ucsa.examen.api.core.services", "py.edu.ucsa.examen.api.core.dao"})
// Es lo mismo que anotar con @Configuration @EnableAutoConfiguration
// @ComponentScan
@Import(JpaConfiguration.class)
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}