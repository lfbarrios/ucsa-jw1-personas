package py.edu.ucsa.examen.api.core.dao.impl;

import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import py.edu.ucsa.examen.api.core.dao.AbstractDao;
import py.edu.ucsa.examen.api.core.dao.TipoDocumentoDao;
import py.edu.ucsa.examen.api.core.model.TipoDocumento;

@Repository("tipoDocumentoDao")
public class TipoDocumentoDaoImpl extends AbstractDao<Integer, TipoDocumento> implements TipoDocumentoDao {

	@Override
	public TipoDocumento getById(Integer id) {
		TipoDocumento td = super.getById(id);
		return td;
	}

	@Override
	public TipoDocumento getByCodigo(String codigo) {
		logger.debug("Código: " + codigo);
		try {
			TipoDocumento td = (TipoDocumento) getEntityManager()
					.createQuery("SELECT td FROM TipoDocumento td WHERE td.codigo = :codigo")
					.setParameter("codigo", codigo).getSingleResult();
			return td;
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public void insertar(TipoDocumento tipoDocumento) {
		super.persistir(tipoDocumento);
	}

	@Override
	public void actualizar(TipoDocumento tipoDocumento) {
		super.actualizar(tipoDocumento);
	}

	@Override
	public void borrarPorCodigo(String codigo) {
		TipoDocumento td = (TipoDocumento) getEntityManager()
				.createQuery("SELECT td FROM TipoDocumento td WHERE td.codigo = :codigo").setParameter("codigo", codigo)
				.getSingleResult();
		eliminar(td);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<TipoDocumento> listarTodos() {
		List<TipoDocumento> tipoDocumento = getEntityManager()
				.createQuery("SELECT td FROM TipoDocumento td ORDER BY td.codigo ASC").getResultList();
		return tipoDocumento;
	}

}
