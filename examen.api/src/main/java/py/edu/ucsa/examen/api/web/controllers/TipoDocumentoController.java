package py.edu.ucsa.examen.api.web.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;
import py.edu.ucsa.examen.api.core.model.TipoDocumento;
import py.edu.ucsa.examen.api.core.services.TipoDocumentoService;
import py.edu.ucsa.examen.api.util.ErrorDTO;

@RestController
@RequestMapping("/tipodocumento")
public class TipoDocumentoController {

	public static final Logger logger = LoggerFactory.getLogger(TipoDocumentoController.class);

	// ESTE SERVICE HARÁ TODAS LAS TAREAS DE
	// RECUPERACIÓN Y MANIPULACIÓN DE DATOS
	@Autowired
	private TipoDocumentoService tipoDocumentoService;

	// ================ RECUPERAMOS TODOS LOS TIPOS DE DOCUMENTO ================ 
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarTiposDocumento() {
		List<TipoDocumento> tipoDocumento = tipoDocumentoService.listarTodos();
		if (tipoDocumento.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<TipoDocumento>>(tipoDocumento, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN TIPO DE DOCUMENTO A PARTIR DE SU ID
	// ================
	@RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getTipoDocumentoPorId(@PathVariable("id") Integer id) {
		logger.info("Vamos a obtener el tipo de documento con id {}.", id);
		TipoDocumento tipoDocumento = tipoDocumentoService.obtenerTipoDocumentoPorId(id);
		if (tipoDocumento == null) {
			logger.error("No se encontró ningún tipo de documento con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ningún tipo de documento con id " + id),
					HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<TipoDocumento>(tipoDocumento, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UN TIPO DE DOCUMENTO A PARTIR DE SU CÓDIGO
	// ================
	@RequestMapping(value = "/codigo/{codigo}", method = RequestMethod.GET)
	public ResponseEntity<?> getTipoDocumentoPorCodigo(@PathVariable("codigo") String codigo) {
		logger.info("Vamos a obtener el tipo de documento con código {}.", codigo);
		TipoDocumento tipoDocumento = tipoDocumentoService.obtenerTipoDocumentoPorCodigo(codigo);
		if (tipoDocumento == null) {
			logger.error("No se encontró ningún tipo de documento con código {}.", codigo);
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("No se encontró ningún tipo de documento con código " + codigo), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<TipoDocumento>(tipoDocumento, HttpStatus.OK);
	}

	// ================ CREAMOS UN TIPO DE DOCUMENTO ================
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearTipoDocumento(@RequestBody TipoDocumento tipoDocumento,
			UriComponentsBuilder ucBuilder) {
		logger.info("Creando el TipoDocumento : {}", tipoDocumento);
		if (tipoDocumentoService.isExisteTipoDocumentoPorCodigo(tipoDocumento)) {
			logger.error("Inserción fallida. Ya existe un tipo de documento con el código {}",
					tipoDocumento.getCodigo());
			return new ResponseEntity<ErrorDTO>(new ErrorDTO(
					"Inserción fallida. Ya existe un tipo de documento con el código " + tipoDocumento.getCodigo()),
					HttpStatus.CONFLICT);

		}
		if (tipoDocumentoService.isExisteTipoDocumentoPorId(tipoDocumento)) {
			logger.error("Inserción fallida. Ya existe un tipo de documento con el id {}", tipoDocumento.getId());
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO(
							"Inserción fallida. Ya existe un tipo de documento con el id " + tipoDocumento.getId()),
					HttpStatus.CONFLICT);

		}
		tipoDocumentoService.guardarTipoDocumento(tipoDocumento);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/tipoDocumento/{id}").buildAndExpand(tipoDocumento.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UN TIPO DE DOCUMENTO
	// ================
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarTipoDocumentoPorId(@PathVariable("id") Integer id,
			@RequestBody TipoDocumento tipoDocumento) {
		logger.info("Actualizando el tipo de documento con id {}", id);
		TipoDocumento tipoDocumentoBD = tipoDocumentoService.obtenerTipoDocumentoPorId(id);
		if (tipoDocumentoBD == null) {
			logger.error("Actualización fallida. No existe el tipo de documento con el id {}.", id);
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe el tipo de documento con el id " + id),
					HttpStatus.NOT_FOUND);

		}
		tipoDocumentoBD.setCodigo(tipoDocumento.getCodigo());
		tipoDocumentoBD.setDescripcion(tipoDocumento.getDescripcion());
		tipoDocumentoBD.setEstado(tipoDocumento.getEstado());
		tipoDocumentoService.guardarTipoDocumento(tipoDocumentoBD);
		return new ResponseEntity<TipoDocumento>(tipoDocumentoBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UN TIPO DE DOCUMENTO POR SU CÓDIGO
	// ================
	@RequestMapping(value = "/{codigo}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarTipoDocumentoPorCodigo(@PathVariable("codigo") String codigo) {
		logger.info("Obteniendo y eliminando el tipo de documento con código {}", codigo);
		TipoDocumento tipoDocumento = tipoDocumentoService.obtenerTipoDocumentoPorCodigo(codigo);
		if (tipoDocumento == null) {
			logger.error("Eliminación fallida. No existe el tipo de documento con el código {}", codigo);
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Eliminación fallida. No existe el tipo de documento con el código " + codigo),
					HttpStatus.NOT_FOUND);

		}
		tipoDocumentoService.eliminarTipoDocumentoPorCodigo(codigo);
		return new ResponseEntity<TipoDocumento>(HttpStatus.NO_CONTENT);
	}
}
