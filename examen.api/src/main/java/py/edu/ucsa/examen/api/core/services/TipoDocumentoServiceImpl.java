package py.edu.ucsa.examen.api.core.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import py.edu.ucsa.examen.api.core.dao.TipoDocumentoDao;
import py.edu.ucsa.examen.api.core.model.TipoDocumento;

@Service("tipoDocumentoService")
@Transactional
public class TipoDocumentoServiceImpl implements TipoDocumentoService {

	@Autowired
	private TipoDocumentoDao dao;

	@Override
	public TipoDocumento obtenerTipoDocumentoPorId(Integer id) {
		return dao.getById(id);
	}

	@Override
	public TipoDocumento obtenerTipoDocumentoPorCodigo(String codigo) {
		return dao.getByCodigo(codigo);
	}

	@Override
	public void guardarTipoDocumento(TipoDocumento tipoDocumento) {
		if (tipoDocumento.getCodigo() != null)
			dao.insertar(tipoDocumento);
		else
			dao.actualizar(tipoDocumento);
	}

	@Override
	public void modificarTipoDocumento(TipoDocumento tipoDocumento) {
		dao.actualizar(tipoDocumento);
	}

	@Override
	public void eliminarTipoDocumentoPorCodigo(String codigo) {
		dao.borrarPorCodigo(codigo);
	}

	@Override
	public List<TipoDocumento> listarTodos() {
		return dao.listarTodos();
	}
	
	@Override
	public boolean isExisteTipoDocumentoPorCodigo(TipoDocumento tipoDocumento) {
		return dao.getByCodigo(tipoDocumento.getCodigo()) != null;
	}
	
	@Override
	public boolean isExisteTipoDocumentoPorId(TipoDocumento tipoDocumento) {
		return dao.getById(tipoDocumento.getId()) != null;
	}

}
