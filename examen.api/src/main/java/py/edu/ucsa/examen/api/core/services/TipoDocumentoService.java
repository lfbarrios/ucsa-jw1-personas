package py.edu.ucsa.examen.api.core.services;

import java.util.List;
import py.edu.ucsa.examen.api.core.model.TipoDocumento;

public interface TipoDocumentoService {
	
	TipoDocumento obtenerTipoDocumentoPorId(Integer id);
	
	TipoDocumento obtenerTipoDocumentoPorCodigo(String codigo);

	void guardarTipoDocumento(TipoDocumento tipoDocumento);

	void modificarTipoDocumento(TipoDocumento tipoDocumento);

	void eliminarTipoDocumentoPorCodigo(String codigo);

	List<TipoDocumento> listarTodos();
	
	boolean isExisteTipoDocumentoPorCodigo(TipoDocumento tipoDocumento);
	
	boolean isExisteTipoDocumentoPorId(TipoDocumento tipoDocumento);

}
