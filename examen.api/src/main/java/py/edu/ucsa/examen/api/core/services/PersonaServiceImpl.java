package py.edu.ucsa.examen.api.core.services;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.edu.ucsa.examen.api.core.dao.PersonaDao;
import py.edu.ucsa.examen.api.core.model.Persona;

@Service("personaService")
@Transactional
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonaDao dao;

	@Override
	public Persona obtenerPersonaPorId(Integer id) {
		return dao.getById(id);
	}

	@Override
	public Persona obtenerPersonaPorNumeroDocumento(String nroDoc) {
		return dao.getByNumeroDocumento(nroDoc);
	}

	@Override
	public void guardarPersona(Persona persona) {
		if (persona.getnroDoc() != null) {
			dao.insertar(persona);
		} else {
			dao.actualizar(persona);
		}
	}

	@Override
	public void modificarPersona(Persona persona) {
		dao.actualizar(persona);
	}

	@Override
	public void eliminarPersonaPorNumeroDocumento(String nroDoc) {
		dao.borrarPorNumeroDocumento(nroDoc);
	}

	@Override
	public List<Persona> listarTodos() {
		return dao.listarTodos();
	}

	@Override
	public boolean isExistePersonaPorNumeroDocumento(Persona persona) {
		return dao.getByNumeroDocumento(persona.getnroDoc()) != null;
	}

	@Override
	public boolean isExistePersonaPorId(Persona persona) {
		return dao.getById(persona.getId()) != null;
	}

}
