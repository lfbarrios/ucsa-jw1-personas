package py.edu.ucsa.examen.api.web.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;
import py.edu.ucsa.examen.api.core.model.Persona;
import py.edu.ucsa.examen.api.core.services.PersonaService;
import py.edu.ucsa.examen.api.util.ErrorDTO;
@CrossOrigin
@RestController
@RequestMapping("/persona")
public class PersonaController {

	public static final Logger logger = LoggerFactory.getLogger(PersonaController.class);

	// ESTE SERVICE HARÁ TODAS LAS TAREAS DE
	// RECUPERACIÓN Y MANIPULACIÓN DE DATOS
	@Autowired
	private PersonaService personaService;

	// ================ RECUPERAMOS TODAS LAS PERSONAS ================ 
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> listarPersona() {
		List<Persona> persona = personaService.listarTodos();
		if (persona.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Persona>>(persona, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UNA PERSONA A PARTIR DE SU ID
	// ================
	@RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getPersonaPorId(@PathVariable("id") Integer id) {
		logger.info("Vamos a obtener a la persona con id {}.", id);
		Persona persona = personaService.obtenerPersonaPorId(id);
		if (persona == null) {
			logger.error("No se encontró ninguna persona con id {}.", id);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO("No se encontró ninguna persona con id " + id),
					HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}

	// ================ RECUPERAMOS UNA PERSONA A PARTIR DE SU NÚMERO DE DOCUMENTO
	// ================
	@RequestMapping(value = "/numerodocumento/{numerodocumento}", method = RequestMethod.GET)
	public ResponseEntity<?> getPersonaPorNumeroDocumento(@PathVariable("numerodocumento") String numerodocumento) {
		logger.info("Vamos a obtener a la persona con número de documento {}.", numerodocumento);
		Persona persona = personaService.obtenerPersonaPorNumeroDocumento(numerodocumento);
		if (persona == null) {
			logger.error("No se encontró ninguna persona con número de documento {}.", numerodocumento);
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("No se encontró ninguna persona con número de documento " + numerodocumento),
					HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}

	// ================ CREAMOS UNA PERSONA ================
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<?> crearPersona(@RequestBody Persona persona, UriComponentsBuilder ucBuilder) {
		logger.info("Creando la persona : {}", persona);
		if (personaService.isExistePersonaPorNumeroDocumento(persona)) {
			logger.error("Inserción fallida. Ya existe una persona con el número de documento {}",
					persona.getnroDoc());
			return new ResponseEntity<ErrorDTO>(new ErrorDTO(
					"Inserción fallida. Ya existe una persona con el número de documento " + persona.getnroDoc()),
					HttpStatus.CONFLICT);

		}
//		if (personaService.isExistePersonaPorId(persona)) {
//			logger.error("Inserción fallida. Ya existe una persona con el id {}", persona.getId());
//			return new ResponseEntity<ErrorDTO>(
//					new ErrorDTO("Inserción fallida. Ya existe una persona con el id " + persona.getId()),
//					HttpStatus.CONFLICT);
//
//		}
		personaService.guardarPersona(persona);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/persona/{id}").buildAndExpand(persona.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ================ ACTUALIZAMOS LOS DATOS DE UNA PERSONA
	// ================
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> actualizarPersonaPorId(@PathVariable("id") Integer id, @RequestBody Persona persona) {
		logger.info("Actualizando datos de la persona con id {}", id);
		Persona personaBD = personaService.obtenerPersonaPorId(id);
		if (personaBD == null) {
			logger.error("Actualización fallida. No existe una persona con el id {}.", id);
			return new ResponseEntity<ErrorDTO>(
					new ErrorDTO("Actualización fallida. No existe una persona con el id " + id), HttpStatus.NOT_FOUND);

		}
		personaBD.setidTipoDoc(persona.getidTipoDoc());
		personaBD.setnroDoc(persona.getnroDoc());
		personaBD.setNombres(persona.getNombres());
		personaBD.setApellidos(persona.getApellidos());
		personaBD.setFisica(persona.isFisica());
		personaBD.setDireccion(persona.getDireccion());
		personaBD.setTelefono(persona.getTelefono());
		personaBD.setCelular(persona.getCelular());
		personaService.guardarPersona(personaBD);
		return new ResponseEntity<Persona>(personaBD, HttpStatus.OK);
	}

	// ================ ELIMINAMOS UNA PERSONA POR SU NÚMERO DE DOCUMENTO
	// ================
	@RequestMapping(value = "/{numerodocumento}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminarPersonaPorNumeroDocumento(
			@PathVariable("numerodocumento") String numerodocumento) {
		logger.info("Obteniendo y eliminando la persona con número de documento {}", numerodocumento);
		Persona persona = personaService.obtenerPersonaPorNumeroDocumento(numerodocumento);
		if (persona == null) {
			logger.error("Eliminación fallida. No existe ninguna persona con el número de documento {}",
					numerodocumento);
			return new ResponseEntity<ErrorDTO>(new ErrorDTO(
					"Eliminación fallida. No existe ninguna persona con el número de documento " + numerodocumento),
					HttpStatus.NOT_FOUND);
		}
		personaService.eliminarPersonaPorNumeroDocumento(numerodocumento);
		return new ResponseEntity<Persona>(HttpStatus.NO_CONTENT);
	}
}
