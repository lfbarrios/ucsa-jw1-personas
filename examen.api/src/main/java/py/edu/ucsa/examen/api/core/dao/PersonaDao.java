package py.edu.ucsa.examen.api.core.dao;

import java.util.List;
import py.edu.ucsa.examen.api.core.model.Persona;

public interface PersonaDao {

	Persona getById(Integer id);

	Persona getByNumeroDocumento(String nroDoc);

	void insertar(Persona persona);

	void actualizar(Persona persona);

	void borrarPorNumeroDocumento(String nroDoc);

	List<Persona> listarTodos();

}
