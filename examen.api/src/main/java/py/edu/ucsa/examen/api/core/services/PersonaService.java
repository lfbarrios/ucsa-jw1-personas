package py.edu.ucsa.examen.api.core.services;

import java.util.List;

import py.edu.ucsa.examen.api.core.model.Persona;

public interface PersonaService {

	Persona obtenerPersonaPorId(Integer id);

	Persona obtenerPersonaPorNumeroDocumento(String nroDoc);

	void guardarPersona(Persona persona);

	void modificarPersona(Persona persona);

	void eliminarPersonaPorNumeroDocumento(String nroDoc);

	List<Persona> listarTodos();

	boolean isExistePersonaPorNumeroDocumento(Persona persona);

	boolean isExistePersonaPorId(Persona persona);

}
